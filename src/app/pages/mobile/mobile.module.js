(function () {
  'use strict';

  angular.module('BlurAdmin.pages.mobile', [

  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('mobile', {
          url: '/mobile',
          abstract: true,
          template: '<div ui-view  autoscroll="true" autoscroll-body-top></div>',
          title: 'Mobile-compare',
          sidebarMeta: {
            icon: 'ion-stats-bars',
            order: 150,
          },
        })
        .state('mobile.home', {
          url: '/home',
          templateUrl: 'app/pages/mobile/home/mobile91.html',
          controller : 'mobilectr',
          title: 'Home',
          sidebarMeta: {
            icon: 'ion-stats-bars',
            order: 150,
          },
        });
  }

})();
