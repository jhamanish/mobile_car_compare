(function () {
  'use strict';

  angular.module('BlurAdmin.pages.mobile').factory("homepageService", ["$timeout", "$q", "$http", '$filter',"$log",
  function($timeout,$q,$http,$filter,$log){
    var displayProduct = [];
    var setDisplayProduct = function(arg){
      displayProduct.push(arg);
    }
    var getDisplayProduct = function(){
      return displayProduct;
    }



    var getMessages = function(start,number,params) {
     var deferred = $q.defer();

     $timeout(function() {
       $http.get("app/pages/Json/car_product.json").then(
         function(response){
          var filtered = params.search.predicateObject ? $filter('filter')(response.data, params.search.predicateObject) : response.data;
          if (params.sort.predicate) {
             filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
           }
          var result = filtered.slice(start, start + number);

        deferred.resolve({
             data: result
           });

         },
         function(err) {
             deferred.reject(err);
         }
      );
     }, 2000);

     return deferred.promise;
   };
   var mobileAttribute = function(){

      var deferred = $q.defer();

      $timeout(function(){
           $http.get('app/pages/Json/Mobile.json')
               .success(function(response){
                  var MobileAttribute = response;
                 console.log(response);
                  deferred.resolve({
                      data:response
                  });
              })
               .error(function(message){
                  deferred.reject(message);
                  $log.error(message);
              });
      },2000);

      return deferred.promise;
  };

   var getMobileData = function() {
    var deferred = $q.defer();

    $timeout(function() {
      $http.get("app/pages/Json/Mobile_product.json").then(
        function(response){
          deferred.resolve({
            data: response.data
          });
        },
        function(err) {
            deferred.reject(err);
        }
     );
    }, 2000);

    return deferred.promise;
  };


    return {
      "getMessages" : getMessages,
      "getDisplayProduct" : getDisplayProduct,
      "setDisplayProduct" : setDisplayProduct,
        "getMobileAttribute":mobileAttribute,
      "getMobileData" : getMobileData
    }


  }]);

})()
