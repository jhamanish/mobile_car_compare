(function () {
  'use strict';

  angular.module('BlurAdmin.pages.mobile').factory("mobileService", ["$timeout", "$q", "$http", '$filter',"$log",
  function($timeout,$q,$http,$filter,$log){


   var getMobileData = function() {
    var deferred = $q.defer();

    $timeout(function() {
      $http.get("app/pages/mobile/json/mobile.json").then(
        function(response){
          deferred.resolve({
            data: response.data
          });
        },
        function(err) {
            deferred.reject(err);
        }
     );
    }, 2000);

    return deferred.promise;
  };


    return {

      "getMobileData" : getMobileData
    }


  }]);

})()
