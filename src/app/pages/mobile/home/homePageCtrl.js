(function () {
  'use strict';

  angular.module('BlurAdmin.pages.mobile')
  .controller("homePageCtrl",homePageCtrl).controller("mobilectr",mobilectr);

  function mobilectr($scope,mobileService){
  mobileService.getMobileData().then(function(arg){
    $scope.mobiledata = arg.data;
    console.log(  $scope.mobiledata);
  });

  }

  function homePageCtrl($scope, homepageService){
    $scope.showCarAttribute1 = false;$scope.showCarAttribute2 = false;$scope.showCarAttribute3 = false;$scope.showCarAttribute4 = false;




    $scope.showAttribute1 = function(){
      $scope.showCarAttribute1 = true;
    }
    $scope.showAttribute2 = function(){
      $scope.showCarAttribute2 = true;
    }
    $scope.showAttribute3 = function(){
      $scope.showCarAttribute3 = true;
    }
    $scope.showAttribute4 = function(){
      $scope.showCarAttribute4 = true;
    }

    $scope.removeProduct1 = function(){
      $scope.display1 = null;
      $scope.buttonshow1 = false;
      $scope.statechange1 = '';
    }
    $scope.removeProduct2 = function(){
      $scope.display2 = null;
      $scope.buttonshow2 = false;
      $scope.statechange2 = '';
    }
    $scope.removeProduct3 = function(){
      $scope.display3 = null;
      $scope.buttonshow3 = false;
      $scope.statechange3 = '';
    }
    $scope.removeProduct4 = function(){
      $scope.display4 = null;
      $scope.buttonshow4 = false;
      $scope.statechange4 = '';
    }

    $scope.showAttribute = false;
    $scope.pushProduct1 = function($item, $model, $label){

        $scope.display1 = $item;
        $scope.buttonshow1 = true;
    }
    $scope.pushProduct2 = function($item, $model, $label){

        $scope.display2 = $item;
        $scope.buttonshow2 = true;
    }
    $scope.pushProduct3 = function($item, $model, $label){

        $scope.display3 = $item;
        $scope.buttonshow3 = true;
    }
    $scope.pushProduct4 = function($item, $model, $label){

        $scope.display4 = $item;
        $scope.buttonshow4 = true;
    }


    	homepageService.getMobileData().then(function(arg){
      		$scope.MobileProduct = arg.data;
          console.log($scope.MobileProduct);
	     });
       homepageService.getMobileAttribute().then(function(arg){
          $scope.MobileAttribute = arg.data;
      });
}

})();
